var width = window.innerWidth;
var height = window.innerHeight;
const SHAPE_HEIGHT = 100;
const STROKE_WIDTH = 4;

const createSquare = (positionX, positionY, color) => {
    return new Konva.Rect({
        x: positionX,
        y: positionY,
        width: SHAPE_HEIGHT,
        height: SHAPE_HEIGHT,
        fill: color,
        stroke: 'black',
        strokeWidth: STROKE_WIDTH
    });
};

const createCircle = (positionX, positionY, color) => {
    return new Konva.Circle({
        x: positionX,
        y: positionY,
        radius: SHAPE_HEIGHT / 2,
        fill: color,
        stroke: 'black',
        strokeWidth: STROKE_WIDTH,
        draggable: true
    });
};

const intersectCircleWithSquare = (circle, square) => {
    let testX = circle.x, testY = circle.y;
    if (circle.x < square.x) {
        testX = square.x;
    }
    else if (circle.x > square.x + square.width) {
        testX = square.x + square.width;
    }

    if (circle.y < square.y) {
        testY = square.y;
    }
    else if (circle.y > square.y + square.width) {
        testY = square.y + square.width;
    }

    let distanceX = circle.x - testX;
    let distanceY = circle.y - testY;
    let distance = Math.sqrt((distanceX * distanceX) + (distanceY * distanceY));

    return distance <= circle.width;
};

const getSquare = (squareX, sqareY) => {
    return {
        x: squareX,
        y: sqareY,
        width: SHAPE_HEIGHT
    };
};

const getCurrentCircle = (e) => {
    return {
        x: e.target.absolutePosition().x,
        y: e.target.absolutePosition().y,
        width: SHAPE_HEIGHT / 2
    };
};

const centerCurrentCircleInSquare = (e, squareX, squareY) => {
    e.target.setAttrs({
        x: squareX + SHAPE_HEIGHT / 2,
        y: squareY + SHAPE_HEIGHT / 2
    });
};

const resetCurrentCirclePosition = (e, circleX, circleY) => {
    e.target.setAttrs({
        x: circleX,
        y: circleY
    });
}

let stage = new Konva.Stage({
    container: 'canvas',
    width: width,
    height: height,
});

let layer = new Konva.Layer();

let redSquareX = stage.width() / 4 - SHAPE_HEIGHT / 2;
let redSquareY = stage.height() / 4 - SHAPE_HEIGHT / 2;
let blueSquareX = 3 * stage.width() / 4 - SHAPE_HEIGHT / 2;
let blueSquareY = stage.height() / 4 - SHAPE_HEIGHT / 2;

let redCircleX = stage.width() / 4;
let redCircleY = 3 * stage.height() / 4;
let blueCircleX = 3 * stage.width() / 4;
let blueCircleY = 3 * stage.height() / 4;

let redSquare = createSquare(redSquareX, redSquareY, "red");
let blueSquare = createSquare(blueSquareX, blueSquareY, "blue");

let redCircle = createCircle(redCircleX, redCircleY, "red");
let blueCircle = createCircle(blueCircleX, blueCircleY, "blue");
let redCircleIsDraggable = true;
let blueCircleIsDraggable = true;

layer.add(redSquare);
layer.add(blueSquare);
layer.add(redCircle);
layer.add(blueCircle);

redCircle.on('dragstart', function (e) {
    e.target.draggable(redCircleIsDraggable);
}).on('dragend', function (e) {
    let square = getSquare(redSquareX, redSquareY);
    let circle = getCurrentCircle(e);

    if (intersectCircleWithSquare(circle, square)) {
        centerCurrentCircleInSquare(e, redSquareX, redSquareY);
        redCircleIsDraggable = false;
    }
    else {
        resetCurrentCirclePosition(e, redCircleX, redCircleY);
    }
    stage.draw();
})

blueCircle.on('dragstart', function (e) {
    e.target.draggable(blueCircleIsDraggable);
}).on('dragend', function (e) {
    let square = getSquare(blueSquareX, blueSquareY);
    let circle = getCurrentCircle(e);

    if (intersectCircleWithSquare(circle, square)) {
        centerCurrentCircleInSquare(e, blueSquareX, blueSquareY);
        blueCircleIsDraggable = false;
    }
    else {
        resetCurrentCirclePosition(e, blueCircleX, blueCircleY);
    }
    stage.draw();
})

stage.add(layer);