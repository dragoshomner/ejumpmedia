let myData = [];
let firstSelectSet = new Set();
let secondSelectSet = new Set();
let thirdSelectSet = new Set();

const splitMyData = (lines) => {
    let words = [];
    lines.forEach(function (line) {
        words.push(line.split(/,/));
    });
    return words;
};

const initializeDataContainer = (lines) => {
    lines.forEach(function (line) {
        $("#data-container").append("<div>" + line + "</div>");
    });
};

const initializeSelects = () => {
    myData.forEach(function (data) {
        firstSelectSet.add(data[0]);
        secondSelectSet.add(data[1]);
        thirdSelectSet.add(data[2]);
    })
    firstSelectSet.forEach(function (item) {
        $("#selectA").append("<option value='" + item + "'>" + item + "</option>");
    });
    secondSelectSet.forEach(function (item) {
        $("#selectB").append("<option value='" + item + "'>" + item + "</option>");
    });
    thirdSelectSet.forEach(function (item) {
        $("#selectC").append("<option value='" + item + "'>" + item + "</option>");
    });
};


$(document).ready(function () {
    $.get('../resources/testData.txt')
        .done(function (data) {
            let lines = data.split(/\n|\r\n/);
            initializeDataContainer(lines);
            myData = splitMyData(lines);
            initializeSelects();
        })
        .fail(function () {
            alert("File not found");
        });
});

const updateDataContainer = (valueA, valueB, valueC) => {
    let result = [];
    myData.forEach(function (item) {
        if (valueA.includes(item[0]) && valueB.includes(item[1]) && valueC.includes(item[2])) {
            result.push(item);
        }
    })
    if (result.length > 0) {
        result.forEach(function (item) {
            $("#data-container").append("<div>" + item[0] + "," + item[1] + "," + item[2] + "</div>");
        });
    }
    else {
        $("#data-container").append("<div>No results found.</div>");
    }
    return result;
};

const getPossibleValues = (value, set) => {
    return value === 'all' ? Array.from(set) : [value];
}

const updateSelect = (selectId, set) => {
    $(selectId).empty();
    console.log(set.length);
    if (set.size > 1) {
        $(selectId).append("<option value=\"all\">Toate</option>");
    }
    set.forEach(function (item) {
        $(selectId).append("<option value='" + item + "'>" + item + "</option>");
    });
}

const updateAllSelects = (data) => {
    let selectAValuesSet = new Set();
    let selectBValuesSet = new Set();
    let selectCValuesSet = new Set();
    data.forEach(function (item) {
        selectAValuesSet.add(item[0]);
        selectBValuesSet.add(item[1]);
        selectCValuesSet.add(item[2]);
    });
    updateSelect("#selectA", selectAValuesSet);
    updateSelect("#selectB", selectBValuesSet);
    updateSelect("#selectC", selectCValuesSet);
};

$("#selectA, #selectB, #selectC").change(function () {
    let valueA = $("#selectA").val();
    let valueB = $("#selectB").val();
    let valueC = $("#selectC").val();
    $("#data-container").empty();

    let possibleValueA = getPossibleValues(valueA, firstSelectSet);
    let possibleValueB = getPossibleValues(valueB, secondSelectSet);
    let possibleValueC = getPossibleValues(valueC, thirdSelectSet);
    let filteredData = updateDataContainer(possibleValueA, possibleValueB, possibleValueC);
    updateAllSelects(filteredData);
})