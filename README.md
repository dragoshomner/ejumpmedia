# Exercitii Internship eJump Media

**Exercitiul 1**

In cadrul acestui exercitiu am folosit libraria Konva, care ma ajuta sa manageriez mai usor layer-ele si shape-urile. Am initializat patratele si cercurile in catre un cadran
al ecranului, cele din urma avand atributul de 'draggable'. Dupa, ma folosesc de event-urile 'dragstart' si 'dragend' pentru fiecare cerc, iar daca se intersecteaza, lucru
verificat de functia *intersectCircleWithSquare* (verifica daca distanta euclidiana intre centrul cercului si cel mai apropiat colt este mai mica sau egala decat raza cercului),
atunci ii setez atributul de 'draggable' al cercului in *false*. Daca nu, setez pozitia acestuia folosind coordonatele principale.

**Exercitiul 2**

Citesc datele din fisierul *resources/testData.txt*, le afisez in select-uri si dedesupt. Pentru a le pune in select, folosesc cate un set pentru a retine valorile unice.
La fiecare seletare, imi iau un array aditional in care tin minte valorile ce indeplinesc conditiile din select. Pe baza acestor date, imi completez si optiunile select-urilor.
Daca selectul are mai mult de o optiune, adaug si optiunea "Toate". 

**Exercitiul 3**

La partea la a treia am folosit framowork-ul Bootstrap pentru a duce la indeplinire task-ul. Iconitele sunt luate de pe fontawesome, iar font-urile de pe Google Fonts si Online Web
Fonts. Imaginile folosite, care au fost exportate din proiectul de Photoshop se gasesc in *resources/images*.

